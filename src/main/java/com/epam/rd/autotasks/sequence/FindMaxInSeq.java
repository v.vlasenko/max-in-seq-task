package com.epam.rd.autotasks.sequence;

import java.util.Scanner;

public class FindMaxInSeq {

    public static int max() {

        Scanner scanner = new Scanner(System.in);
        int inputNumber = scanner.nextInt();
        int max = inputNumber;

        for (int i = 0; inputNumber != 0; i++) {
            max = Math.max(inputNumber, max);
            inputNumber = scanner.nextInt();
        }
        return max;
    }

    public static void main(String[] args) {
        System.out.println(max());
    }
}

